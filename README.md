# Engine

Experimental design where events are made available to subscribe to, plus a big focus on RAII even to wrap C library calls for low-level engine functionality (creating visible window, etc.).
