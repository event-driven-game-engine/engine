#pragma once
#include <vector>

struct Subscriber;
struct Event
{
    std::vector<Subscriber*> subscribers;
    void Trigger ();
};

struct Subscriber
{
    virtual void Execute () = 0;
    int pos_in_vec;
    Event* event;
    Subscriber (Event* subscribe_to);
    virtual ~Subscriber ();
};

// Convenience implementation for a C style function pointer.
struct Subscriber_Func : Subscriber
{
    void (*func) (void* userdata);
    void* userdata;
    Subscriber_Func (Event* subscribe_to, void (*func) (void* userdata), void* userdata);
    void Execute () override;
};
