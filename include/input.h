#pragma once
#include "event.h"
#include <unordered_map>

struct InputDef
{
    int keycode;
    static InputDef KeyCode (int keycode);
};

bool operator== (InputDef const& a, InputDef const& b);
template<> struct std::hash<InputDef>
{
    size_t operator() (InputDef const& input) const noexcept;
};

struct InputService : Subscriber
{
    bool Exiting = false;
    InputService (Event* game_frame);
    std::unordered_map<InputDef, Event*> input_events;
    Event* GetEvent (InputDef input_def);
    void FireEvent (InputDef received);
    ~InputService ();
    void Execute () override;
};

struct InputSubscriber : Subscriber
{
    InputSubscriber (InputDef def);
};
