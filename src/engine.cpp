#include "engine.h"
#include <glad/glad.h>
using namespace std;

#define GAME_CODE_PATH "game.dll"

EngineError::EngineError (string reason) : reason(reason) {}
const char* EngineError::what () const noexcept
{
    return reason.c_str();
}

Engine::LibSDL2::LibSDL2 ()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) throw EngineError("Can't initialize SDL2.");
}
Engine::LibSDL2::~LibSDL2 ()
{
    SDL_Quit();
}

Engine::Window::Window ()
{
    GameWindow = SDL_CreateWindow("test",
                                  SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                  1280, 720,
                                  SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
    if (!GameWindow) throw EngineError("Can't create window.");
}
Engine::Window::~Window ()
{
    SDL_DestroyWindow(GameWindow);
}

Engine::GLContext::GLContext (SDL_Window* window)
{
    Context = SDL_GL_CreateContext(window);
    if (!Context) throw EngineError("Can't create OpenGL context.");
}
Engine::GLContext::~GLContext ()
{
	SDL_GL_DeleteContext(Context);
}

Engine::LoadGL::LoadGL ()
{
    if (!gladLoadGL()) throw EngineError("Can't load OpenGL functions.");
    if (GLVersion.major < 3) throw EngineError("OpenGL minimal functionality not met.");
}

Engine::GameCode::GameCode ()
{
    code_ptr = SDL_LoadObject(GAME_CODE_PATH);
    if (!code_ptr) throw EngineError("Can't load game code.");
}
Engine::GameCode::~GameCode ()
{
    SDL_UnloadObject(code_ptr);
}

Engine::Engine () : GLContextInstance(WindowInstance.GameWindow)
{
    EngineInstance = this;
    GameCode game_code;
    try
    {
        GameStart.Trigger();
        while (!InputServiceInstance.Exiting) GameFrame.Trigger();
        GameClosing.Trigger();
    }
    catch (...)
    {
        throw EngineError("Unhandled exception from game.");
    }
}
Engine* EngineInstance = NULL;
