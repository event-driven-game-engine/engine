#include "engine.h"
#include <SDL2/SDL.h>
#include <stdexcept>
using namespace std;

InputDef InputDef::KeyCode (int keycode)
{
    return {keycode};
}

bool operator== (InputDef const& a, InputDef const& b)
{
    return a.keycode == b.keycode;
}
size_t hash<InputDef>::operator() (InputDef const& input) const noexcept
{
    return hash<int>{}(input.keycode);
}

InputService::InputService (Event* game_frame) : Subscriber(game_frame) {}
Event* InputService::GetEvent (InputDef input_def)
{
    try
    {
        return input_events.at(input_def);
    }
    catch (out_of_range nope)
    {
        Event* created = new Event;
        input_events[input_def] = created;
        return created;
    }
}
void InputService::FireEvent (InputDef received)
{
    try
    {
        input_events.at(received)->Trigger();
    }
    catch (out_of_range nope) {}
}
InputService::~InputService ()
{
    for (pair<InputDef, Event*> pair : input_events)
    {
        delete pair.second;
    }
}
void InputService::Execute ()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT) Exiting = true;
        if (event.type == SDL_KEYDOWN) FireEvent(InputDef::KeyCode(event.key.keysym.sym));
    }
}

InputSubscriber::InputSubscriber (InputDef def) : Subscriber(EngineInstance->InputServiceInstance.GetEvent(def)) {}
