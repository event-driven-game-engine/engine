#include "required-service.h"

RequiredService::RequiredService (std::string dll_name)
{
    code_ptr = SDL_LoadObject(dll_name.c_str());
    if (!code_ptr) throw EngineError("Can't load required service.");
}
RequiredService::~RequiredService ()
{
    SDL_UnloadObject(code_ptr);
}
